import React from 'react';

const Route = props => (
  <li className={`route ${props.route}`} data-route={props.route} onClick={props.onClickRoute}>
    {props.route}
  </li>
);

export default Route;
