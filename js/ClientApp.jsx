import React, { Component } from 'react';
import { render } from 'react-dom';
import axios from 'axios';
import Map from './Map';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      routeList: ['j', 'l', 'm', 'n'],
      activeRoute: {}
    };
    this.onClickRoute = this.onClickRoute.bind(this);
  }

  componentWillMount() {
    setInterval(() => {
      for (let route in this.state.activeRoute) {
        if(this.state.activeRoute[route]){
          this.fetchData(route);
        }
      }
    }, 5000);
  }

  onClickRoute(evt) {
    const route = evt.target.getAttribute('data-route');
    const activeRoute = Object.assign({}, this.state.activeRoute);
 
    if (this.state.activeRoute[route]) {
      activeRoute[route] = null;
      this.setState({ activeRoute });
    } else {
      this.fetchData(route);
    }
  }

  fetchData(route) {
    const url = `http://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a=sf-muni&r=${route.toUpperCase()}&t=${'1144953500233'}`;
    const activeRoute = Object.assign({}, this.state.activeRoute);

    axios.get(url).then(vehicleLocations => {
      activeRoute[route] = vehicleLocations.data.vehicle;
      this.setState({ activeRoute });
    });
  }

  render() {
    return (
      <div className="app">
        <Map
          height={1200}
          width={900}
          routeList={this.state.routeList}
          onClickRoute={this.onClickRoute}
          activeRoute={this.state.activeRoute}
        />
      </div>
    );
  }
}

render(<App />, document.getElementById('app'));
