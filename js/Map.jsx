import React, { Component } from 'react';
import * as d3 from 'd3';
import RouteList from './Routelist';
import neighborhoodsData from '../geo_json/neighborhoods.json';

class Map extends Component {

  componentDidMount() {
    //init map of SF with neighborhood
    const node = this.node;
    d3.select(node).append('g');

    const projection = d3
      .geoMercator()
      .center([-122.487385538782686, 37.74455359294009])
      .scale(350000)
      .translate([this.props.width / 2, this.props.height / 2]);

    const path = d3.geoPath().projection(projection);

    d3
      .select('g')
      .append('path')
      .attr('d', path(neighborhoodsData))
      .attr('fill', 'transparent')
      .attr('stroke', 'grey')
      .attr('stroke-width', '0.5');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeRoute) {
      this.renderMap(nextProps);
    }
  }

  renderMap(props) {
    const activeRoute = props.activeRoute;
    const routeList = props.routeList;
    const projection = d3
      .geoMercator()
      .center([-122.487385538782686, 37.74455359294009])
      .scale(350000)
      .translate([props.width / 2, props.height / 2]);

    //renders with data of routes and vehicles
    for (let route in activeRoute) {
      if (activeRoute[route]) {
        displayRoute(route);
        updateVehicles(route, activeRoute[route]);
      } else {
        deleteAll(route);
      }
    }

    function displayRoute(route) {
      const routeUrl = `http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=sf-muni&r=${route.toUpperCase()}`;

      d3.json(routeUrl, function(data) {
        const routes = d3.select('g').selectAll('route').data(data.route.stop);

        routes
          .enter()
          .append('circle', 'route')
          .classed(`stop ${route}`, true)
          .attr('r', 2)
          .merge(routes)
          .attr('transform', function(d) {
            return `translate(${projection([d.lon, d.lat])})`;
          });

        routes.exit().remove();
      });
    }

    function updateVehicles(route, vehicleData) {
      const vehicles = d3.select('g').selectAll(`.${route}`).data(vehicleData);

      vehicles
        .enter()
        .append('rect')
        .classed(`${route}`, true)
        .attr('width', 10)
        .attr('height', 5)
        .attr('fill', 'green')
        .merge(vehicles)
        .attr('transform', function(d) {
          return `translate(${projection([d.lon, d.lat])})`;
        });

      vehicles.exit().remove();
    }

    function deleteAll(route) {
      const routeEl = d3.select('g').selectAll(`.${route}`);

      routeEl.remove();
    }
  }

  render() {
    return (
      <div>
        <RouteList routeList={this.props.routeList} onClickRoute={this.props.onClickRoute} />
        <svg ref={node => (this.node = node)} width={this.props.width} height={this.props.height} />
      </div>
    );
  }
}

export default Map;
