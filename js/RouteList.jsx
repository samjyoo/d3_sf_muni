import React from 'react';
import Route from './Route';

const RouteList = props => {
  const route = props.routeList.map(route => <Route key={route} route={route} onClickRoute={props.onClickRoute} />);

  return (
    <div>
      <ul className="routes-list">
        {route}
      </ul>
    </div>
  );
};

export default RouteList;
